FROM node:latest
RUN mkdir -p --verbose /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app

RUN ls
RUN npm upgrade -g npm
RUN npm upgrade -g yarn
RUN yarn global add serve
RUN yarn
RUN yarn build

EXPOSE 5000

RUN ls
CMD serve -s build