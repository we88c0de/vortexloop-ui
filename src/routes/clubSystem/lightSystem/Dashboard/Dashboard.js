import React from "react";
import {Breadcrumb, Card} from "antd";

const Dashboard = () => {
  return (
    <Card className="gx-card" title="Light System - DashboardX">
      <Breadcrumb>
        <Breadcrumb.Item><span className="gx-link"><a href="/">Home</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system">Club System</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system/light-system">Light System</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system/light-system/dashboard">Dashboard</a></span></Breadcrumb.Item>        
      </Breadcrumb>
    </Card>
  );
};

export default Dashboard;