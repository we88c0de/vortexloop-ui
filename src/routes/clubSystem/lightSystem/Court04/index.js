import React from "react";
import {Col, Row} from "antd";

import Court04 from "./Court04";

const court04Base = () => {
  return (
    <Row>
      <Col lg={12} md={12} sm={24} xs={24}>
        <Court04 />
      </Col>
    </Row>
  );
};
export default court04Base;
