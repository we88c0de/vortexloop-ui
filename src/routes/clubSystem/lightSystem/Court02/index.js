import React from "react";
import {Col, Row} from "antd";

import Court02 from "./Court02";

const court02Base = () => {
  return (
    <Row>
      <Col lg={6} md={6} sm={12} xs={12}>
        <Court02 /> 123
      </Col>
    </Row>
  );
};
export default court02Base;
