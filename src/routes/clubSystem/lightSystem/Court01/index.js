import React from "react";
import {Card, Col, Row, Calendar} from "antd";

import Court01 from "./Court'";


const CalendarCard = () => {
  function onPanelChange(value, mode) {
    console.log(value, mode);
  }
};


const court01Base = () => {
  return (
    <Row>
      <Col lg={6} md={6} sm={12} xs={12}>
        123
      </Col>
      <Col lg={3} md={3} sm={6} xs={6}>
        <Card className="gx-card gx-com-calendar-card" title="CalendarCard">
          <Calendar className="gx-com-calendar" fullscreen={false} onPanelChange={onPanelChange}/>
        </Card>
      </Col>
    </Row>    
  );
};
export default court01Base;
