import React from "react";
import {Breadcrumb, Card} from "antd";

const Court01 = () => {
  return (
    <Card className="gx-card" title="Light System - Court 01">
      <Breadcrumb>
        <Breadcrumb.Item><span className="gx-link"><a href="/">Home</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system">Club System</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system/light-system">Light System</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system/light-system/court01">Court 01</a></span></Breadcrumb.Item>        
      </Breadcrumb>
    </Card>
  );
};

export default Court01;