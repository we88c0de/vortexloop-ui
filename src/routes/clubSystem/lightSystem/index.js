import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from "util/asyncComponent";

const lightSystemBase = ({match}) => (
  <Switch>    
    <Route exact path={`${match.url}/`} component={asyncComponent(() => import('./LightSystem'))}/>
    <Route exact path={`${match.url}/Dashboard`} component={asyncComponent(() => import('./Dashboard/Dashboard'))}/>    
    <Route exact path={`${match.url}/Court01`} component={asyncComponent(() => import('./Court01/Court01'))}/>
    <Route exact path={`${match.url}/Court02`} component={asyncComponent(() => import('./Court02/index'))}/>
    <Route exact path={`${match.url}/Court03`} component={asyncComponent(() => import('./Court03/Court03'))}/>
    <Route exact path={`${match.url}/Court04`} component={asyncComponent(() => import('./Court04/Court04'))}/>     
  </Switch>
);

export default lightSystemBase;
