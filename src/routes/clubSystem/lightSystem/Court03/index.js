import React from "react";
import {Col, Row} from "antd";

import Court03 from "./Court03";
import Court02 from "./Court02";

const court03Base = () => {
  return (
    <Row>
      <Col lg={12} md={12} sm={24} xs={24}>
        <Court03 />
        <Court02 />
      </Col>
    </Row>
  );
};
export default court03Base;
