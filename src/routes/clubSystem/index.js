import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from "util/asyncComponent";


const clubSystemBase = ({match}) => (
  <Switch>          
    <Route exact path={`${match.url}/`} component={asyncComponent(() => import('./ClubSystem'))}/> 
    <Route exact path={`${match.url}/light-system`} component={asyncComponent(() => import('./lightSystem'))}/>
    <Route exact path={`${match.url}/light-system/Dashboard`} component={asyncComponent(() => import('./lightSystem/Dashboard/Dashboard'))}/>
    <Route exact path={`${match.url}/light-system/Court01`} component={asyncComponent(() => import('./lightSystem/Court01/Court01'))}/>
    <Route exact path={`${match.url}/light-system/Court02`} component={asyncComponent(() => import('./lightSystem/Court02/Court02'))}/>
    <Route exact path={`${match.url}/light-system/Court03`} component={asyncComponent(() => import('./lightSystem/Court03/Court03'))}/>
    <Route exact path={`${match.url}/light-system/Court04`} component={asyncComponent(() => import('./lightSystem/Court04/Court04'))}/>   
  </Switch>
);

export default clubSystemBase;
