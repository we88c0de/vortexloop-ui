import React from "react";
import {Breadcrumb, Card} from "antd";

const ClubSystem = () => {
  return (
    <Card className="gx-card" title="Club System Overview">
      <Breadcrumb>
        <Breadcrumb.Item><span className="gx-link"><a href="/">Home</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system">Club System</a></span></Breadcrumb.Item>                 
      </Breadcrumb>
    </Card>
  );
};

export default ClubSystem;