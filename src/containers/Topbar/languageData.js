const languageData = [
  {
    languageId: 'englishMT',
    locale: 'enMT',
    name: 'English (MT)',
    icon: 'mt'
  }
];
export default languageData;
