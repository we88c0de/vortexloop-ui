import antdEn from "antd/lib/locale-provider/en_US";
import enMessages from "../locales/en_MT.json";

const EnMtLang = {
  messages: {
    ...enMessages
  },
  antd: antdEn,
  locale: 'en-MT'
};
export default EnMtLang;
